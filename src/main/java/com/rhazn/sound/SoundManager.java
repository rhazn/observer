package com.rhazn.sound;

import com.rhazn.Main;
import javazoom.jl.player.advanced.AdvancedPlayer;

import java.util.HashMap;

public class SoundManager {
    protected HashMap<String, AdvancedPlayer> sounds = new HashMap<>();
    private static SoundManager ourInstance = new SoundManager();

    public static SoundManager getInstance() {
        return ourInstance;
    }

    private SoundManager() {
    }

    public void play(String key) {
        try {
            getPlayerForKey(key).play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AdvancedPlayer getPlayerForKey(String key) throws Exception {
        return new AdvancedPlayer(Main.class.getClassLoader().getResourceAsStream(String.format("sound/female/%s.mp3", key)));
    }
}
