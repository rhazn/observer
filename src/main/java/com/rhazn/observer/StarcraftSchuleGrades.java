package com.rhazn.observer;

import com.rhazn.gui.TrayManager;
import com.rhazn.sound.SoundManager;
import com.rhazn.upload.ReplayUploader;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Observable;
import java.util.Observer;

public class StarcraftSchuleGrades implements Observer {
    int toonId;

    public StarcraftSchuleGrades(int toonId) {
        this.toonId = toonId;
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("Getting Grades from SCS.");
        ReplayUploader uploader = (ReplayUploader) o;
        HttpUriRequest gradeRequest = null;
        try {
            gradeRequest = RequestBuilder.post()
                    .setUri(new URI("http://www.starcraftschule.de/scs_api/my_replays"))
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        CloseableHttpResponse response = null;
        try {
            response = uploader.getHttpClient().execute(gradeRequest);
            String responseContent = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            JSONObject responseJson = new JSONObject(responseContent);
            JSONObject latestReplay = responseJson.getJSONArray("replays").getJSONObject(0);
            JSONObject player = latestReplay.getJSONArray("players").getJSONObject(0).getInt("toon_id") == toonId ? latestReplay.getJSONArray("players").getJSONObject(0) : latestReplay.getJSONArray("players").getJSONObject(1);

            JSONObject supplyBlock = player.getJSONObject("supplyBlock");
            JSONObject spending = player.getJSONObject("spending");

            String overallGrade = getPrivateGrade(supplyBlock.getString("grade"), spending.getString("grade"));
            String gradeMessage = String.format("Supplyblock: %s\nSpending: %s\nOverall: %s", supplyBlock.getString("grade"), spending.getString("grade"),overallGrade);

            TrayManager.getInstance().displayMessage("Grades", gradeMessage);

            SoundManager.getInstance().play("supplyblock");
            if (supplyBlock.getString("grade").length() > 1) {
                SoundManager.getInstance().play(supplyBlock.getString("grade").substring(0,1));
                SoundManager.getInstance().play(supplyBlock.getString("grade").substring(1).equals("+") ? "plus" : "minus");
            } else {
                SoundManager.getInstance().play(supplyBlock.getString("grade"));
            }

            SoundManager.getInstance().play("spending");
            if (spending.getString("grade").length() > 1) {
                SoundManager.getInstance().play(spending.getString("grade").substring(0,1));
                SoundManager.getInstance().play(spending.getString("grade").substring(1).equals("+") ? "plus" : "minus");
            } else {
                SoundManager.getInstance().play(spending.getString("grade"));
            }

            SoundManager.getInstance().play("overall");
            if (overallGrade.length() > 1) {
                SoundManager.getInstance().play(overallGrade.substring(0, 1));
                SoundManager.getInstance().play(overallGrade.substring(1).equals("+") ? "plus" : "minus");
            } else {
                SoundManager.getInstance().play(overallGrade);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getPrivateGrade(String supplyblockGrade, String spendingGrade) {
        int supplyblockGradeInt;
        if (supplyblockGrade.length() > 1){
            supplyblockGradeInt = Integer.parseInt(supplyblockGrade.substring(0, 1)) * 2;

            if (supplyblockGrade.substring(1).equals("+")) {
                supplyblockGradeInt -= 1;
            } else {
                supplyblockGradeInt += 1;
            }
        } else {
            supplyblockGradeInt = Integer.parseInt(supplyblockGrade) * 2;
        }

        int spendingGradeInt;
        if (spendingGrade.length() > 1){
            spendingGradeInt = Integer.parseInt(spendingGrade.substring(0, 1)) * 2;

            if (spendingGrade.substring(1).equals("+")) {
                spendingGradeInt -= 1;
            } else {
                spendingGradeInt += 1;
            }
        } else {
            spendingGradeInt = Integer.parseInt(spendingGrade) * 2;
        }

        int overallGrade = (spendingGradeInt + supplyblockGradeInt) / 2;

        switch (overallGrade) {
            case 0:
                return "1+";
            case 1:
                return "1";
            case 2:
                return "1-";
            case 3:
                return "2+";
            case 4:
                return "2";
            case 5:
                return "2-";
            case 6:
                return "3+";
            case 7:
                return "3";
            case 8:
                return "3-";
            case 9:
                return "4+";
            case 10:
                return "4";
            case 11:
                return "4-";
            case 12:
                return "5+";
            case 13:
                return "5";
            case 14:
                return "5-";
            case 15:
                return "6+";
            case 16:
                return "6";
            case 17:
                return "6-";
        }

        return "0";
    }
}
