package com.rhazn.observer;

import com.rhazn.upload.ReplayUploader;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Observable;
import java.util.Observer;

public class StarcraftSchuleReplayPublic implements Observer {
    CloseableHttpClient httpClient;

    @Override
    public void update(Observable o, Object arg) {
        ReplayUploader uploader = (ReplayUploader) o;
        httpClient = uploader.getHttpClient();
        HttpUriRequest uploadRequest = null;
        try {
            uploadRequest = RequestBuilder.post()
                    .setUri(new URI("http://www.starcraftschule.de/scs_api/my_replays"))
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(uploadRequest);
            JSONObject responseJson = new JSONObject(EntityUtils.toString(response.getEntity()));
            JSONObject latestReplay = responseJson.getJSONArray("replays").getJSONObject(0);
            String replayHash = latestReplay.getString("replayHash");
            EntityUtils.consumeQuietly(response.getEntity());

            makeReplayPublic(replayHash);

            System.out.println("Making replay " + replayHash + " public.");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected void makeReplayPublic(String replayHash) {
        HttpUriRequest makePublicRequest = null;
        try {
            makePublicRequest = RequestBuilder.get()
                    .setUri(new URI("http://www.starcraftschule.de/replay_public/" + replayHash))
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(makePublicRequest);
            EntityUtils.consumeQuietly(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
