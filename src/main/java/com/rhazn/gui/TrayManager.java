package com.rhazn.gui;

import com.rhazn.Main;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

public class TrayManager {
    private static TrayManager ourInstance = new TrayManager();
    protected TrayIcon trayIcon;

    public static TrayManager getInstance() {
        return ourInstance;
    }

    private TrayManager() {
        Image image = null;
        try {
            image = ImageIO.read(Main.class.getClassLoader().getResource("trayicon.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        trayIcon = new TrayIcon(image, "Observer");

        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();

            trayIcon.setImageAutoSize(true);

            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.err.println("TrayIcon could not be added.");
            }
        }
    }

    public void displayMessage(String title, String msg) {
        if (SystemTray.isSupported()) {
            trayIcon.displayMessage(title, msg, TrayIcon.MessageType.INFO);
        }
    }
}
