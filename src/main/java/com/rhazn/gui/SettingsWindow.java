package com.rhazn.gui;

import com.rhazn.Main;

import javax.swing.*;
import java.awt.*;

public class SettingsWindow extends JFrame {

    JPanel pane;

    Settings settings;

    JTextField replayFolder;
    JTextField username;
    JPasswordField password;
    JTextField toonId;
    JCheckBox uploadReplay;
    JButton saveButton;

    public SettingsWindow() {
        super("Observer Settings");
        ImageIcon img = new ImageIcon(Main.class.getClassLoader().getResource("trayicon.png"));
        this.setIconImage(img.getImage());

        settings = Settings.getInstance();
        settings.setSettingsWindow(this);

        setBounds(100, 100, 300, 400);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = getContentPane();

        pane = new JPanel();
        pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));

        pane.add(getDisclaimer());
        pane.add(getReplayFolderInput());
        pane.add(getSCSUsernameInput());
        pane.add(getSCSPasswordInput());
        pane.add(getToonIdInput());
        pane.add(getReplayPublicInput());
        pane.add(getSaveInput());

        container.add(pane);

        setVisible(true);
    }

    private JComponent getReplayFolderInput() {
        JPanel replayFolderPanel = new JPanel();
        replayFolderPanel.setLayout(new BoxLayout(replayFolderPanel, BoxLayout.Y_AXIS));

        replayFolder = new JTextField("", 20);
        replayFolder.setText(settings.getReplayFolderPath());

        JLabel replayfolderLabel = new JLabel("SC II Replay Folder");
        replayfolderLabel.setLabelFor(replayFolder);

        replayFolderPanel.add(replayfolderLabel);
        replayFolderPanel.add(replayFolder);

        return replayFolderPanel;
    }

    private JComponent getSCSUsernameInput() {
        JPanel usernamePanel = new JPanel();
        usernamePanel.setLayout(new BoxLayout(usernamePanel, BoxLayout.Y_AXIS));

        username = new JTextField("", 20);
        username.setText(settings.getUsername());

        JLabel usernameLabel = new JLabel("SCS Username");
        usernameLabel.setLabelFor(username);

        usernamePanel.add(usernameLabel);
        usernamePanel.add(username);

        return usernamePanel;
    }

    private JComponent getSCSPasswordInput() {
        JPanel passwordPanel = new JPanel();
        passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.Y_AXIS));

        password = new JPasswordField("", 20);
        password.setText(settings.getPassword());

        JLabel passwordLabel = new JLabel("SCS Password");
        passwordLabel.setLabelFor(password);

        passwordPanel.add(passwordLabel);
        passwordPanel.add(password);

        return passwordPanel;
    }

    private JComponent getToonIdInput() {
        JPanel toonIdPanel = new JPanel();
        toonIdPanel.setLayout(new BoxLayout(toonIdPanel, BoxLayout.Y_AXIS));

        toonId = new JTextField("", 20);
        toonId.setText(settings.getToonId());

        JLabel toonIdLabel = new JLabel("SCS toonId");
        toonIdLabel.setLabelFor(toonId);

        toonIdPanel.add(toonIdLabel);
        toonIdPanel.add(toonId);

        return toonIdPanel;
    }

    private JComponent getReplayPublicInput() {
        JPanel replayPublicPanel = new JPanel();
        replayPublicPanel.setLayout(new BoxLayout(replayPublicPanel, BoxLayout.Y_AXIS));

        uploadReplay = new JCheckBox();
        uploadReplay.setSelected(settings.uploadReplays());

        JLabel replayPublicLabel = new JLabel("Make replay public?");
        replayPublicLabel.setLabelFor(uploadReplay);

        replayPublicPanel.add(replayPublicLabel);
        replayPublicPanel.add(uploadReplay);

        return replayPublicPanel;
    }

    private JComponent getSaveInput() {
        JPanel savePanel = new JPanel();
        savePanel.setLayout(new BoxLayout(savePanel, BoxLayout.Y_AXIS));

        saveButton = new JButton("Save & Start");
        saveButton.addActionListener(settings);

        savePanel.add(saveButton);

        return savePanel;
    }

    private JComponent getDisclaimer() {
        JPanel disclaimerPanel = new JPanel();
        disclaimerPanel.setLayout(new BoxLayout(disclaimerPanel, BoxLayout.Y_AXIS));

        JLabel disclaimer = new JLabel();
        disclaimer.setText("<html><body><p style=\"width:200px\">This is no well working software, do not use it. I am in no way" +
                "affiliated with the Starcraftschule. You should not enter your password into this program. Even" +
                "though it is not saved, it is still an external program. Don't. If you want to talk, find" +
                "me on twitter @rhanarion.<br/><br/><br/></p></body></html>");

        disclaimerPanel.add(disclaimer);

        return disclaimerPanel;
    }

    public JTextField getReplayFolder() {
        return replayFolder;
    }

    public JTextField getUsername() {
        return username;
    }

    public JPasswordField getPassword() {
        return password;
    }

    public JTextField getToonId() {
        return toonId;
    }

    public JCheckBox getUploadReplay() {
        return uploadReplay;
    }

    public JButton getSaveButton() {
        return saveButton;
    }
}
