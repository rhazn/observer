package com.rhazn.gui;

import com.rhazn.Main;
import com.rhazn.Observer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;

public class Settings implements ActionListener {
    private static Settings ourInstance = new Settings();

    protected Properties properties = new Properties();
    protected SettingsWindow settingsWindow;
    protected Observer observer;
    protected String password = "";

    public static Settings getInstance() {
        return ourInstance;
    }

    private Settings() {
        properties = new Properties();
        InputStream input = null;

        try {
            input = Main.class.getClassLoader().getResourceAsStream("config.properties");

            // load a properties file
            properties.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void setSettingsWindow(SettingsWindow settingsWindow) {
        this.settingsWindow = settingsWindow;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        properties.setProperty("filepath", settingsWindow.getReplayFolder().getText());
        properties.setProperty("username", settingsWindow.getUsername().getText());
        password = String.valueOf(settingsWindow.getPassword().getPassword());
        properties.setProperty("toonId", settingsWindow.getToonId().getText());
        properties.setProperty("replayPublic", settingsWindow.getUploadReplay().isSelected() ? "1" : "0");

        OutputStream out = null;
        try {
            URL resourceUrl = Main.class.getClassLoader().getResource("config.properties");
            File file = new File(resourceUrl.toURI());
            out = new FileOutputStream(file);

            // save a properties file
            properties.store(out, "");
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        }

        if (observer == null) {
            observer = new Observer(this);
        }

        settingsWindow.getSaveButton().setEnabled(false);
        settingsWindow.getSaveButton().setText("Running");
    }

    public String getReplayFolderPath() {
        return properties.getProperty("filepath");
    }

    public String getUsername() {
        return properties.getProperty("username");
    }

    public String getPassword() {
        return password;
    }

    public String getToonId() {
        return properties.getProperty("toonId");
    }

    public boolean uploadReplays() {
        return properties.getProperty("replayPublic").equals("1");
    }
}
