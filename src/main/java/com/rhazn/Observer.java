package com.rhazn;

import com.rhazn.file.FileWatcher;
import com.rhazn.gui.Settings;
import com.rhazn.observer.StarcraftSchuleGrades;
import com.rhazn.observer.StarcraftSchuleReplayPublic;
import com.rhazn.upload.ReplayUploader;
import com.rhazn.upload.StarcraftSchuleUploader;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Observer {
    protected Settings settings;
    Thread watchThread;
    Thread uploadThread;

    public Observer(Settings settings) {
        this.settings = settings;

        BlockingQueue<File> replayFiles = new LinkedBlockingQueue<File>();

        // setup file watcher
        watchThread = new Thread(new FileWatcher(settings.getReplayFolderPath(), replayFiles));
        watchThread.start();

        // setup replay upload
        ReplayUploader starcraftSchule = new StarcraftSchuleUploader(replayFiles, settings.getUsername(), settings.getPassword());

        uploadThread = new Thread(starcraftSchule);
        uploadThread.start();

        // setup reaction to upload
        StarcraftSchuleGrades starcraftSchuleGrades = new StarcraftSchuleGrades(Integer.parseInt(settings.getToonId()));
        starcraftSchule.addObserver(starcraftSchuleGrades);

        if (settings.uploadReplays()) {
            StarcraftSchuleReplayPublic starcraftSchuleReplayPublic = new StarcraftSchuleReplayPublic();
            starcraftSchule.addObserver(starcraftSchuleReplayPublic);
        }
    }
}
