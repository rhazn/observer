package com.rhazn;

import com.rhazn.file.FileWatcher;
import com.rhazn.gui.SettingsWindow;
import com.rhazn.gui.TrayManager;
import com.rhazn.observer.StarcraftSchuleGrades;
import com.rhazn.observer.StarcraftSchuleReplayPublic;
import com.rhazn.upload.ReplayUploader;
import com.rhazn.upload.StarcraftSchuleUploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) {
        // setup tray icon
        TrayManager.getInstance();

        // show settings gui
        new SettingsWindow();
    }
}
