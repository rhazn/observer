package com.rhazn.upload;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.BlockingQueue;

public class StarcraftSchuleUploader extends ReplayUploader {
    protected String username;
    protected String password;

    //todo remove
    protected boolean reallyUpload = true;

    public StarcraftSchuleUploader(BlockingQueue<File> queue, String username, String password) {
        super(queue);

        this.username = username;
        this.password = password;
    }

    public boolean isLoggedIn() {
        return !this.cookieStore.getCookies().isEmpty();
    }

    public boolean uploadReplay(File replay) throws Exception {
        if (!isLoggedIn()) {
            boolean loginSuccessful = login(this.username, this.password);

            if (!loginSuccessful) {
                throw new Exception("Login failed");
            }
        }

        System.out.println("uploading file: " + replay.getAbsolutePath());
        MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
        multipartEntityBuilder.addBinaryBody("file", replay);

        HttpEntity content = multipartEntityBuilder.build();

        HttpUriRequest uploadRequest = RequestBuilder.post()
                .setUri(new URI("http://www.starcraftschule.de/replay_up"))
                .setEntity(content)
                .build();

        CloseableHttpResponse response = httpClient.execute(uploadRequest);
        EntityUtils.consumeQuietly(response.getEntity());
        response.close();

        if (!reallyUpload || response.getStatusLine().getStatusCode() == 200) {
            System.out.println("notify obs");
            setChanged();
            notifyObservers();
            return true;
        } else {
            return false;
        }
    }

    protected boolean login(String username, String password) throws IOException, URISyntaxException {
        HttpUriRequest login = RequestBuilder.post()
                .setUri(new URI("http://www.starcraftschule.de/home"))
                .addParameter("User[login][username]", username)
                .addParameter("User[login][password]", password)
                .build();

        CloseableHttpResponse response = httpClient.execute(login);

        try {
            HttpEntity entity = response.getEntity();

            String responseContent = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(entity);

            return responseContent.toLowerCase().contains(username.toLowerCase());
        } finally {
            response.close();
        }
    }
}
