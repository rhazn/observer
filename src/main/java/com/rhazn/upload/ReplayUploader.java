package com.rhazn.upload;

import com.rhazn.gui.TrayManager;
import com.rhazn.sound.SoundManager;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.File;
import java.util.Observable;
import java.util.concurrent.BlockingQueue;

public abstract class ReplayUploader extends Observable implements Runnable {
    protected BlockingQueue<File> replayFilesQueue;
    protected CloseableHttpClient httpClient;
    protected BasicCookieStore cookieStore;

    public ReplayUploader(BlockingQueue<File> queue) {
        this.replayFilesQueue = queue;
        this.cookieStore = new BasicCookieStore();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(15000).setConnectionRequestTimeout(15000).build();
        this.httpClient = HttpClients.custom().setDefaultCookieStore(this.cookieStore).setDefaultRequestConfig(requestConfig).build();
    }

    public abstract boolean uploadReplay(File replay) throws Exception;

    @Override
    public void run() {
        File replayFile;

        while(true) {
            replayFile = replayFilesQueue.poll();

            if (replayFile != null && !replayFile.getName().contains("writeCacheBackup")) {
                try {
                    TrayManager.getInstance().displayMessage("Upload", "Uploading Replay...");
                    SoundManager.getInstance().play(Math.random() >= 0.5 ? "gg" : "goodgame");
                    SoundManager.getInstance().play("upload");
                    uploadReplay(replayFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }
}
