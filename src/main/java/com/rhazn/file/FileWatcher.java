package com.rhazn.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.BlockingQueue;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

public class FileWatcher implements Runnable {
    protected String directory;
    protected WatchKey watchKey;
    protected BlockingQueue<File> queue;

    public FileWatcher(String directory, BlockingQueue<File> queue) {
        this.directory = directory;
        this.queue = queue;

        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path directoryPath = FileSystems.getDefault().getPath(directory);
            watchKey = directoryPath.register(watchService, ENTRY_CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            for (WatchEvent<?> event: watchKey.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();

                // This key is registered only
                // for ENTRY_CREATE events,
                // but an OVERFLOW event can
                // occur regardless if events
                // are lost or discarded.
                if (kind == OVERFLOW) {
                    continue;
                }

                // The filename is the
                // context of the event.
                WatchEvent<Path> ev = (WatchEvent<Path>) event;

                // add file to queue
                Path dir = (Path) watchKey.watchable();
                Path fullPath = dir.resolve(ev.context());
                queue.add(fullPath.toFile());
            }

            // Reset the key -- this step is critical if you want to
            // receive further watch events.  If the key is no longer valid,
            // the directory is inaccessible so exit the loop.
            boolean valid = watchKey.reset();
            if (!valid) {
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
